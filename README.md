# Docker Multiple Compose Skeleton

I recently faced issues with multiple apps running through multiple docker-compose files.  
Problem is when I wanted to load balance servers with nginx, but I wasn't possible because networks between containers wasn't shared.

## Steps (adapt with your choices)
- Add a shared network `main` with `docker network create main` 
- Add `127.0.0.1 node1.test.dev` to `/etc/hosts` 
- Add `127.0.0.1 node2.test.dev` to `/etc/hosts` 
- Go to node1 folder, run `docker-compose up -d`
- Go to node2 folder, run `docker-compose up -d`
- Go to nginx folder, run `docker-compose up -d`
- Try URLs `node1.test.dev` and `node2.test.dev` in your browser, and enjoy

## Nginx config
Nginx configs are in `conf` folder. Each Node.js app got its own `.conf`.  
Docker-compose will sync `conf` host folder with `/etc/nginx/conf.d` nginx container.

## Node.js config
Server config is in `config/config.js` files, with `port` and `https` options.
HTTPS config will come soon.


