let http = require("http");
let https = require("https");
let express = require('express');
let fs = require('fs');
let app = express();

//Internal dependencies
let Config = require('./config/config').config;

let server = http.createServer(app);

app.get('/', function (req, res) {
    res.send('<h1>NODE 1</h1>');
});

//Listening
server.listen(Config.port);
console.log("NODE 1 listening on port " + Config.port);